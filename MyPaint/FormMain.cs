﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace MyPaint
{
    public partial class FormMain : Form
    {
        #region Переменные и константы
        private enum Tool { Empty, Pencil, Line, Rectangle, Ellipse, Text, Erase, Fill }
        private Tool currentTool;

        private Bitmap usedBitmap, savedBitmap, lastBitmap;
        private Graphics graph;

        private Pen pencil;
        private Pen line;
        private Pen rectangle;
        private Pen ellipse;
        private Pen erase;

        private Font textFont;
        private SolidBrush textBrush;
        private string textString;

        private Point startPoint, finishPoint;
        private bool canDraw, canWrite;

        private bool isSaved, hasUnsavedModified;
        private string savedFilePath;

        private const string FILE_NOT_SAVE = "Файл не сохранён";
        #endregion

        #region Вспомогательные процедуры
        private void DrawToPictureBox()
        {
            pictureBox.Image = usedBitmap;
        }

        private void CloneBitmapToAnotherBitmap(Bitmap from, ref Bitmap to)
        {
            to.Dispose();
            to = from.Clone(new Rectangle(0, 0, from.Width, from.Height), from.PixelFormat);
        }

        private void ApplyGraph(Bitmap bitmap)
        {
            if (graph != null) { graph.Dispose(); }
            graph = Graphics.FromImage(bitmap);
        }

        private void UnchekedTools()
        {
            пустойИнструментToolStripMenuItem.Checked = false;
            карандашToolStripMenuItem.Checked = false;
            линияToolStripMenuItem.Checked = false;
            прямоугольникToolStripMenuItem.Checked = false;
            эллипсToolStripMenuItem.Checked = false;
            текстToolStripMenuItem.Checked = false;
            стиркаToolStripMenuItem.Checked = false;
            заливкаToolStripMenuItem.Checked = false;
        }

        private void CheckTool(Tool tool, ToolStripMenuItem item)
        {
            currentTool = tool;
            item.Checked = true;
        }

        #endregion

        public FormMain()
        {
            InitializeComponent();
        }

        #region Обработчики событий формы

        private void FormMain_Load(object sender, EventArgs e)
        {
            новыйToolStripMenuItem_Click(null, null);
        }
        
        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (currentTool != Tool.Empty)
            {
                canWrite = !canWrite;

                if (canWrite == false && currentTool == Tool.Text)
                {
                    CloneBitmapToAnotherBitmap(usedBitmap, ref savedBitmap);
                    textString = string.Empty;
                    return;
                }

                canDraw = true;

                startPoint.X = e.X;
                startPoint.Y = e.Y;

                CloneBitmapToAnotherBitmap(savedBitmap, ref lastBitmap);
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (currentTool != Tool.Empty)
            {
                canDraw = false;
                CloneBitmapToAnotherBitmap(usedBitmap, ref savedBitmap);
                hasUnsavedModified = true;
            }
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (canDraw)
            {
                switch (currentTool)
                {
                    case Tool.Pencil:
                        finishPoint.X = e.X;
                        finishPoint.Y = e.Y;

                        graph.DrawLine(pencil, startPoint, finishPoint);

                        startPoint = finishPoint;

                        break;
                    case Tool.Line:
                        finishPoint.X = e.X;
                        finishPoint.Y = e.Y;

                        CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
                        ApplyGraph(usedBitmap);
                        
                        graph.DrawLine(line, startPoint, finishPoint);
                        break;
                    case Tool.Rectangle:
                        finishPoint.X = e.X;
                        finishPoint.Y = e.Y;

                        CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
                        ApplyGraph(usedBitmap);

                        graph.DrawRectangle(rectangle, startPoint.X, startPoint.Y, finishPoint.X - startPoint.X, finishPoint.Y - startPoint.Y);
                        break;
                    case Tool.Ellipse:
                        finishPoint.X = e.X;
                        finishPoint.Y = e.Y;

                        CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
                        ApplyGraph(usedBitmap);

                        graph.DrawEllipse(rectangle, startPoint.X, startPoint.Y, finishPoint.X - startPoint.X, finishPoint.Y - startPoint.Y);
                        break;
                    case Tool.Erase:
                        finishPoint.X = e.X;
                        finishPoint.Y = e.Y;

                        graph.DrawLine(erase, startPoint, finishPoint);

                        startPoint = finishPoint;
                        break;
                    case Tool.Fill:
                        break;
                }
                DrawToPictureBox();
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isSaved == true && hasUnsavedModified == true)
            {
                if (MessageBox.Show("Хотите сохранить изменения?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    сохранитьToolStripMenuItem_Click(null, null);
                }
            }
            if (isSaved == false && hasUnsavedModified == true)
            {
                if (MessageBox.Show("Хотите сохранить файл?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    сохранитьКакToolStripMenuItem_Click(null, null);
                }
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(pictureBox.Image, 0, 0);
        }

        private void FormMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (canWrite == true && currentTool == Tool.Text)
            {
                if (e.KeyChar == 13)
                {
                    textString += "\n";
                }
                else if (e.KeyChar == 8)
                {
                    if (textString.Length > 0)
                    {
                        textString = textString.Remove(textString.Length - 1);
                    }
                }
                else
                {
                    textString += e.KeyChar;
                }

                CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
                ApplyGraph(usedBitmap);

                graph.DrawString(textString, textFont, textBrush, startPoint.X, startPoint.Y);

                DrawToPictureBox();
            }
        }
        #endregion

        #region Файл
        private void новыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usedBitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
            savedBitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
            lastBitmap = new Bitmap(pictureBox.Width, pictureBox.Height);

            ApplyGraph(savedBitmap);
            graph.Clear(Color.White);

            ApplyGraph(usedBitmap);
            graph.Clear(Color.White);

            pencil = new Pen(Color.Black, 1);
            line = new Pen(Color.Black, 1);
            rectangle = new Pen(Color.Black, 1);
            ellipse = new Pen(Color.Black, 1);
            erase = new Pen(Color.White, 10);

            textBrush = new SolidBrush(Color.Black);
            textFont = DefaultFont;
            textString = string.Empty;

            canDraw = false;

            currentTool = Tool.Empty;

            hasUnsavedModified = true;
            isSaved = false;

            toolStripStatusLabelFilePath.Text = FILE_NOT_SAVE;

            UnchekedTools();
            пустойИнструментToolStripMenuItem.Checked = true;

            DrawToPictureBox();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap loadBitmap = (Bitmap)Image.FromFile(openFileDialog.FileName);

                savedBitmap = new Bitmap(loadBitmap, new Size(pictureBox.Width, pictureBox.Height));

                CloneBitmapToAnotherBitmap(savedBitmap, ref lastBitmap);

                CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
                ApplyGraph(usedBitmap);

                DrawToPictureBox();
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isSaved == true)
            {
                hasUnsavedModified = false;
                pictureBox.Image.Save(savedFilePath);
                MessageBox.Show("Файл успешно сохранён!");
            }
            else
            {
                сохранитьКакToolStripMenuItem_Click(null, null);
            }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                hasUnsavedModified = false;
                isSaved = true;
                savedFilePath = saveFileDialog.FileName;
                toolStripStatusLabelFilePath.Text = savedFilePath;

                pictureBox.Image.Save(savedFilePath);

                MessageBox.Show("Файл успешно сохранён!");
            }
        }

        private void печатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printDocument.Print();
        }


        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Инструменты
        private void очиститьХолстToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneBitmapToAnotherBitmap(savedBitmap, ref lastBitmap);

            usedBitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
            savedBitmap = new Bitmap(pictureBox.Width, pictureBox.Height);

            ApplyGraph(savedBitmap);
            graph.Clear(Color.White);

            ApplyGraph(usedBitmap);
            graph.Clear(Color.White);

            DrawToPictureBox();
        }
        private void отменаПоследнегоДействияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloneBitmapToAnotherBitmap(lastBitmap, ref savedBitmap);

            CloneBitmapToAnotherBitmap(savedBitmap, ref usedBitmap);
            ApplyGraph(usedBitmap);

            DrawToPictureBox();
        }

        private void пустойИнструментToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Empty, пустойИнструментToolStripMenuItem);
        }

        private void карандашToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Pencil, карандашToolStripMenuItem);
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormPenSettings(pencil).ShowDialog();
        }

        private void линияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Line, линияToolStripMenuItem);
        }

        private void настройкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FormPenSettings(line).ShowDialog();
        }

        private void прямоугольникToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Rectangle, прямоугольникToolStripMenuItem);
        }
        private void настройкиToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new FormPenSettings(rectangle).ShowDialog();
        }

        private void эллипсToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Ellipse, эллипсToolStripMenuItem);
        }

        private void настройкиToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            new FormPenSettings(ellipse).ShowDialog();
        }

        private void настройкиToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            FormTextSettings fts = new FormTextSettings(textFont, textBrush);
            fts.ShowDialog();
            textFont = (Font)fts.Tag;
        }

        private void текстToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textString = string.Empty;
            UnchekedTools();
            CheckTool(Tool.Text, текстToolStripMenuItem);
            canWrite = false;
        }

        private void стиркаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnchekedTools();
            CheckTool(Tool.Erase, стиркаToolStripMenuItem);
        }

        private void настройкиToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            new FormPenSettings(erase).ShowDialog();
        }
        #endregion

        #region Помощь
        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormHelp().ShowDialog();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormAbout().ShowDialog();
        }
        #endregion
    }
}
